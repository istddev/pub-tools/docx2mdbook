import { readFileSync } from 'fs';
//import { join } from 'path'
import { default as MarkdownIt } from 'markdown-it';


const markdownIt = new MarkdownIt();
const mdContent = readFileSync('pandoc-output/converted-by-pandoc.md', 'utf-8');
const mdLines = markdownIt.parse(mdContent, {});

for (const mdLine of mdLines) {
    console.log(mdLine.type, mdLine.tag, mdLine.level, 'markup = ' + mdLine.markup, mdLine.content);
}